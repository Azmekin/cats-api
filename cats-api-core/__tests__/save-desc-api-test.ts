import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';

const cats: CatMinInfo[] = [{ name: 'Супервывывувава', description: '', gender: 'male' }];

let catId;

const decription="Amogus";
const HttpClient = Client.getInstance();

describe('API сохранения описания', () => {
  beforeAll(async () => {
    try {
      const add_cat_response = await HttpClient.post('core/cats/add', {
        responseType: 'json',
        json: { cats },
      });
      if ((add_cat_response.body as CatsList).cats[0].id) {
        catId = (add_cat_response.body as CatsList).cats[0].id;
      } else throw new Error('Не получилось получить id тестового котика!');
    } catch (error) {
      throw new Error('Не удалось создать котика для автотестов!');
    }
  });

  afterAll(async () => {
    await HttpClient.delete(`core/cats/${catId}/remove`, {
      responseType: 'json',
    });
  });

  it('Сохранить описание коту', async () => {
    const response = await HttpClient.post(`core/cats/save-description`, {
      responseType: 'json',
      json: {
        catId: catId,
        catDescription: decription
      },
    });
    expect(response.statusCode).toEqual(200);

    expect(response.body).toMatchObject({
      id: catId,
      description: decription
    });
  });

});

import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';


let catId;

const HttpClient = Client.getInstance();

describe('API списка по букве', () => {
  beforeAll(async () => {

  });

  afterAll(async () => {
  });

 

  it('Метод получения группировки по буквам', async () => {
    const response = await HttpClient.get('core/cats/allByLetter', {
      responseType: 'json',
    });
    expect(response.statusCode).toEqual(200);

    expect(response.body).toMatchObject({
      groups: expect.arrayContaining([
        expect.objectContaining({
          title: expect.any(String),
          cats: expect.objectContaining({  
        }),
      }),
      ]),   

      
    });
  });

    it('Метод получения группировки по буквам с параметрами', async () => {
      const response = await HttpClient.get('core/cats/allByLetter?limit=1&order=desc&gender=male', {
        responseType: 'json',
      });
      expect(response.statusCode).toEqual(200);
  
      expect(response.body).toMatchObject({
        groups: expect.arrayContaining([
          expect.objectContaining({
            title: expect.any(String),
            cats: expect.objectContaining({  
          }),
        }),
        ]),   
  
        
  

  });
});
})

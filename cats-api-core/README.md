# cats-api-core

Для запуска необходимо передать следующие переменные окружения
* NODE_PORT - порт приложения
* POSTGRES_USER - пользователь pg
* POSTGRES_PASSWORD - пароль пользователя pg
* POSTGRES_DB - БД pg
* POSTGRES_PORT - порт pg
* POSTGRES_HOST - хост pg
* SERVICE_VERSION - версия сервиса
* SWAGGER_BASE_PATH - swagger.basePath
* SWAGGER_HOST - swagger.host
* SWAGGER_SCHEMES - swagger.schemes (http,https)

